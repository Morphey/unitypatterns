﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    
public class SceneManager : MonoBehaviour
{
    private List<CubeCommand> _botCommands = new List<CubeCommand>();
    private Stack<CubeCommand> _botsCompleteCommands = new Stack<CubeCommand>();
    private Coroutine _executeRoutine;
    private Coroutine _undoRoutine;
    private Cube _bot;

    private void Awake()
    {
        _bot = FindObjectOfType<Cube>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            ExecuteCommands();
        else if (Input.GetKeyDown(KeyCode.Backspace))
            UndoCommands();
        else
            CheckForBotCommands();
    }

    private void ExecuteCommands()
    {
        if (_executeRoutine != null)
            return;

        _executeRoutine = StartCoroutine(ExecuteCommandsRoutine());
    }

    private void UndoCommands()
    {
        if (_undoRoutine != null)
            return;

        _undoRoutine = StartCoroutine(UndoCommandsRoutine());
    }

    private void CheckForBotCommands()
    {
        CubeCommand botCommand = CubeInputHandler.HandleInput();

        if (botCommand != null && _executeRoutine == null)
            _botCommands.Add(botCommand);
    }

    private IEnumerator ExecuteCommandsRoutine()
    {
        Debug.Log("Executing...");

        for (int i = 0, count = _botCommands.Count; i < count; i++)
        {
            CubeCommand command = _botCommands[i];

            command.Execute(_bot);
            _botsCompleteCommands.Push(command);
            yield return new WaitForSeconds(0.5f);
        }

        _botCommands.Clear();
        _executeRoutine = null;
    }

    private IEnumerator UndoCommandsRoutine()
    {
        Debug.Log("Undo...");

        for (int i = 0, count = _botsCompleteCommands.Count; i < count; i++)
        {
            _botsCompleteCommands.Pop().Undo(_bot);
            yield return new WaitForSeconds(0.5f);
        }

        _undoRoutine = null;
    }
}
