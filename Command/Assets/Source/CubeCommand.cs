﻿using UnityEngine;

public class CubeCommand
{
    public ExecuteCallback Execute { get; private set; }
    public ExecuteCallback Undo { get; private set; }
    public delegate void ExecuteCallback(Cube entity);
    private readonly string _commandName;

    public CubeCommand(ExecuteCallback executeMethod, ExecuteCallback undoMethod, string name)
    {
        Execute = executeMethod;
        Undo = undoMethod;
        _commandName = name;
    }

    public override string ToString() => _commandName;
}
