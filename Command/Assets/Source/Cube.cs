﻿using UnityEngine;

public class Cube : MonoBehaviour
{
    public enum MoveDirection { Forward, Back };
    public enum RotateDirection { Right, Left };
    private Transform _transform;

    public void Move(MoveDirection dir)
    {
        if(dir == MoveDirection.Forward)
            _transform.position += _transform.forward;
        else if(dir == MoveDirection.Back)
            _transform.position -= _transform.forward;
    }

    public void Rotate(RotateDirection dir)
    {
        if (dir == RotateDirection.Left)
            _transform.Rotate(Vector3.up, -90f);
        else if (dir == RotateDirection.Right)
            _transform.Rotate(Vector3.up, 90f);
    }

    private void Awake()
    {
        _transform = GetComponent<Transform>();
    }
}
