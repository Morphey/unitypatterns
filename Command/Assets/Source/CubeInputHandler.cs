﻿using UnityEngine;

public class CubeInputHandler : MonoBehaviour
{
    public static CubeCommand HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            return RotateToLeft;

        if (Input.GetKeyDown(KeyCode.RightArrow))
            return RotateToRight;

        if (Input.GetKeyDown(KeyCode.UpArrow))
            return MoveForward;

        return null;
    }

    private static readonly CubeCommand MoveForward =
        new CubeCommand(delegate (Cube bot) { bot.Move(Cube.MoveDirection.Forward); },
                       delegate (Cube bot) { bot.Move(Cube.MoveDirection.Back); },
                       nameof(MoveForward));

    private static readonly CubeCommand RotateToLeft =
        new CubeCommand(delegate (Cube bot) { bot.Rotate(Cube.RotateDirection.Left); },
                       delegate (Cube bot) { bot.Rotate(Cube.RotateDirection.Right); },
                       nameof(RotateToLeft));

    private static readonly CubeCommand RotateToRight =
        new CubeCommand(delegate (Cube bot) { bot.Rotate(Cube.RotateDirection.Right); },
                       delegate (Cube bot) { bot.Rotate(Cube.RotateDirection.Left); },
                       nameof(RotateToRight));
}
