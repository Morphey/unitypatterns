﻿using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public Transform TilePrefab;
    public static int MapBoundary = (int)((_mapSize - 1f) * 0.5f);

    private const float _outlinePercent = 0.1f;
    private const int _mapSize = 9;

    private Transform _holder;
    private const string HOLDER_NAME = "Generated Map";

    public void CreateMap()
    {
        CreateHolder();
        Generate();
    }

    private void CreateHolder()
    {
        if (transform.Find(HOLDER_NAME))
            DestroyImmediate(transform.Find(HOLDER_NAME).gameObject);

        _holder = new GameObject(HOLDER_NAME).transform;
        _holder.parent = transform;
    }

    private void Generate()
    {
        for (int y = 0; y < _mapSize; y++)
        {
            for (int x = 0; x < _mapSize; x++)
            {
                Vector3 tilePosition = new Vector3(-_mapSize * 0.5f + 0.5f + x, -_mapSize * 0.5f + 0.5f + y, 0f);
                Transform newTile = Instantiate(TilePrefab, tilePosition, Quaternion.identity);
                newTile.localScale = Vector3.one * (1 - _outlinePercent);
                newTile.parent = _holder;
            }
        }
    }
}
